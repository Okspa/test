<?php
require 'IProductDB.class.php';
class ProductDB implements IProductDB{
        protected $db;
        protected  $_items=[];
        const DB_NAME = 'mydb';
        const DB_PASS = 'admin';
        const DB_HOST = 'localhost';
        const DB_USER = 'admin';
	const DSN = 'mysql:dbname=mydb;host=127.0.0.1 ';

        function __construct()
        {
            $this->db = new PDO(self::DSN, self::DB_USER, self::DB_PASS);
             
        }
        function __destruct()
        {
            unset($this->db);
        }

	function updateProduct ($title, $category, $sum,$id){
                $this->db->quote($title);
                $this->db->quote($category);
                $this->db->quote($sum);
                $this->db->quote($id);
            $sql="UPDATE `product` SET `title`='".$title."',`price`='".$sum."',`category_id`='".$category."' WHERE `id`=$id";
            if($this->db->exec($sql)){
                    return TRUE; 
                }else{
                    return $this->db->errorInfo();
                }
	}
	function saveProduct ($title, $category, $sum){
                $this->db->quote($title);
                $this->db->quote($category);
                $this->db->quote($sum);
                $sql= "INSERT INTO `product`
                    (`title`,`price`, `category_id`) 
                     VALUES ('".$title."','".$sum."','".$category."')";
                if($this->db->query($sql)){
                    return $this->db->lastInsertId(); 
                }else{
                    return $this->db->errorInfo();
                }
            }
        function getProduct ($id=FALSE){
           
            if(!$id){
            $sql="
                SELECT product.title as product, category.title as category, product.id as id, product.price as price, category.id as category_id
                FROM product, category
                WHERE product.category_id = category.id
                ORDER BY category.title";
            }else{
               $sql="
                SELECT product.title as product, category.title as category, product.id as id, product.price as price, category.id as category_id
                FROM product, category
                WHERE product.category_id = category.id
                AND product.id =".$id; 
            }
            
           if($res=$this->db->query($sql)){
                return $this->db2arr($res);
           }else{
               return $this->db->errorInfo();
           }
         
        }
        function getCat(){
           $sql="SELECT `id`, `title` FROM `category`";
             if($res=$this->db->query($sql)){
                return $this->db2arr($res);
           }else{
               return $this->db->errorInfo();
           }
        }
        
        function deleteProduct ($id){
            $sql="DELETE FROM `product` WHERE id=".$id;
            if ($this->db->exec($sql)>0){
                return TRUE;
            }else{
                return FALSE;
            }
        }
        
        protected function db2arr($data){
            $arr = array();
            while ($row=$data->fetch(PDO::FETCH_ASSOC)){
                $arr[]=$row;
            }
            return $arr;
        }
        function UpdateCat($id,$category){
             $sql="UPDATE `product` SET `category_id`='".$category."' WHERE `id`=".$id;
                if($this->db->exec($sql)){
                       return TRUE; 
                    }else{
                       $result= $this->db->errorInfo();
                    }
        }
       
}       
?>