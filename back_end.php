<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'ProductDB.class.php';
$resalt= array();
$error='';
$db= new ProductDB();

if ($_POST['cmd'] == 'save_product'){
    $title= $_POST['title']; 
    $category = $_POST['category'];
    $price = $_POST['price'];
    $last_id= $db->saveProduct($title, $category, $price);
    if($last_id){
        $result = $db->getProduct($last_id);
    }else{
        $result['error']='error';
    };
    echo json_encode($result);
    return 0;
}

if ($_POST['cmd'] == 'update_product'){
    $title= $_POST['title']; 
    $category = $_POST['category'];
    $price = $_POST['price'];
    $id = $_POST['id'];
    $data=$db->updateProduct ($title, $category, $price, $id);
    if($data){
        $result =  $db->getProduct($id);
    }else{
        $result['error']='error';
    }
    echo json_encode($result);
    return 0;
}

if ($_POST['cmd'] == 'del_product'){
    $id= $_POST['id']; 
    $resalt=$db->deleteProduct($id);
    echo json_encode($result);
    return 0;
}
if ($_POST['cmd'] == 'changeCat'){
    $id= $_POST['id']; 
    $category = $_POST['category'];
    if ($db->UpdateCat($id,$category)>0){
        $result=TRUE;
    }else{
         $result=FALSE;
    }
    echo json_encode($result);
    return 0;
}