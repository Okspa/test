<?php
require 'function.php';
$product = new ProductDB() ;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Product</title>
	<link rel="stylesheet" href="css/bootstrap-grid-3.3.1.css">
        <link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/style.css">
        <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript" src="js/function.js"></script>
</head>
<body>
    <div class="windows" id="windows">
        <p>Наименование продукта</p><input type="text" id="new_title"> <br>
        <p>Категория</p><select id="new_cat">
            <?php 
            $res= $product->getCat();
            for($i=0; $i<count($res);$i++){
                ?>
            <option value="<?=$res[$i]['id'];?>"> <?= $res[$i]['title']?></option>
            <?php
            }
            ?>
        </select><br>
        <p>Цена</p><input type="text" id="new_price">
        
    </div>
    <div id="dialog_message" style="display: none;">
        <div id="code_message">
        </div>
    </div>
    <div id="dialog_change" style="display: none;"></div>
	<div class="container">
            <div class="row">
                <div class="col-md-2"><input id ="add_product" type="button" value="+ add Product" onclick="javascript:openwindow()">
                    <div class = "col-md-10"> </div>
                </div>
            </div>
            <br>
		<div class="row">
			<div class="col-md-12">
				<table>
					<thead>
						<tr>
							<th>ID</th>
							<th>Наименование товара</th>
							<th>Категория</th>
							<th>Цена</th>
                                                        <th></th>
                                                        <th></th>
						</tr>
					</thead>
					<tbody>
						<?php 
                                                    $result= $product->getProduct();
                                                    $res= $product->getCat();
                                                    for ($i=0; $i< count($result);$i++){
                                                       ?>
                                            <tr> <td id="id_<?=$result[$i]['id']?>"> <?=$result[$i]['id']?></td>
                                                <td id="title_<?=$result[$i]['id']?>"> <?=$result[$i]['product']?></td>
                                                <td> 
                                                    <select id='<?='cat_'.$result[$i]['id']?>' class="<?=$result[$i]['id']?>" onchange="javascript:changeCat('<?= 'cat_'.$result[$i]['id']?>')">
                                                        <?php 
                                                            for($r=0; $r<count($res);$r++){
                                                            ?>
                                                             <option 
                                                                 <?php if($result[$i]['category_id'] == $res[$r]['id']){
                                                                    echo ' selected '; 
                                                                 }?>
                                                             value="<?=$res[$r]['id'];?>"> <?= $res[$r]['title']?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                                <td id="price_<?=$result[$i]['id']?>"> <?=$result[$i]['price']?></td>
                                                <td><p onclick='javascript:openDialogChange(this);'>Изменить</p></td>
                                                <td><p onclick='javascript:delProduct(<?=$result[$i]['id']?>);'>Удалить</p></td>
                                            </tr>
                                                <?php
                                                }
                                                ?>
                                       
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>