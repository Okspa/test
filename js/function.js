
$(document).ready(function(){
    	$("#windows").dialog({
			modal: true,
			autoOpen: false,
                        width: '50%',
			buttons:{
        	"Сохранить": function(){
          		saveProduct();
        	},
        	"Отмена": function(){
          		$(this).dialog( "close" );
        	}
			}
	}); 
        $("#dialog_change").dialog({
			modal: true,
			autoOpen: false,
                        width: '50%',
			buttons:{
        	"Сохранить": function(){
          		changeProduct();
        	},
        	"Отмена": function(){
          		$(this).dialog( "close" );
        	}
			}
	});
       $("#dialog_message").dialog({
			modal: true,
			autoOpen: false
	});
        
});
function openwindow(){
    $('#windows').dialog('open');
    if ($('#warn').length>0){
      $('#warn').remove();  
    }
}
function saveProduct(){
    
    var title= $('#new_title').val();
    var category= $('#new_cat').val();
    var price= $('#new_price').val();
    var cat_html = $('#new_cat').html();
      if (title !=='' && category !=='' && price !=='' ){
          $.ajax({
		type: 'POST',
		url : '/back_end.php',
		data: 'cmd=save_product&title='+title+'&category='+category+'&price='+price,
		success: function (result_query) {
		
		result=jQuery.parseJSON(result_query);
                
                var html='';
		 if(result['error'] === undefined && result !== 'null'){
                    for (var i=0;i<result.length;i++){
                        var catChange = 'cat_'+result[i]['id'];
                        html += "<tr><td id='id_"+result[i]['id']+"'>"+result[i]['id']+"</td><td id='tittle_"+result[i]['id']+"'>"+result[i]['product']+"</td>";
                        html +="<td> <select id= 'cat_"+ result[i]['category_id']+"' onchange='javascript:changeCat(\""+catChange+"\")'>"+cat_html+"</select></td>";
                        html+="<td id='price_"+result[i]['id']+"'>"+result[i]['price']+"</td><td><p  onclick='javascript:openDialogChange(this);'>Изменить</p></td><td><p onclick='javaskript:delProduct("+result[i]['id']+");'>Удалить</p></td></tr>";
                        $('tbody').append(html);
                       console.log($('#cat_'+result[i]['category_id']+' option[value="'+result[i]['category_id']+'"]'));
                       $('#cat_'+result[i]['category_id']+' option[value="'+result[i]['category_id']+'"]').attr('selected','selected');
                        html='';
                     }
                 }else{
                        $('#code_message').html('Произошла ошибка!');
			$('#dialog_message').dialog('open');
				
		}
		}
       }); 
       $('#windows').dialog( "close" );
     }else{
      if($('#warn').length === 0){
          var html = '<br> <div style="color:red;" id="warn"> Заполните все поля! </div>';
        $('#windows').append(html);}
    }
 }
 function openDialogChange(data){
     
     var parent_data=$(data).parents('tr').children('td');
     var item = new Array(); 
     var category=$(data).parents('tr').children('td').eq(2).val();
     for(i=0;i<$(data).parents('tr').children('td').length;i++){
         item[i]= parent_data.eq(i).html();
        }
     var html='';
     html+="<p id='change_id' name='"+item[0]+"'>ID: "+item[0]+"</p><br>";
     html+="Название: <input type='text' id='change_title' value=' "+ item[1]+"' size='50'><br><br>";
     html+="<p id='change_category'>Категория: "+ item[2]+"</p><br><br>";
     html+=" Цена: <input id='change_price' type='text' value= "+ item[3]+"><br><br>";
     $('#dialog_change').html(html);
     $('#dialog_change').dialog('open');
 }
 
 function changeProduct(){
    var id = $('#change_id').attr('name');
    var category =$('#change_category').children('select').val();
    var price=$('#change_price').val();
    var title=$('#change_title').val();
            $.ajax({
		type: 'POST',
		url : '/back_end.php',
		data: 'cmd=update_product&title='+title+'&category='+category+'&price='+price+'&id='+id,
		success: function (result_query) {
                    result=jQuery.parseJSON(result_query);
                     if(result['error'] === undefined && result !== 'null'){
                        $('#title_'+result[0]['id']).html(result[0]['title']);
                        $('#category_'+result[0]['id']).val(result[0]['category_id']);
                        $('#price_'+result[0]['id']).html(result[0]['price']);
                        $('#code_message').html('Данные успешно изменены!');
                        $('#dialog_message').dialog('open');
                        $('#dialog_change').dialog( "close" );
                     }else{
                         alert(result);
                            $('#code_message').html('Произошла ошибка!');
                            $('#dialog_message').dialog('open');
                     }
		}
       }); 
         
    
 }
 function delProduct(id){
       $.ajax({
		type: 'POST',
		url : '/back_end.php',
		data: 'cmd=del_product&id='+id,
		success: function (result_query) {
                    if(result_query){
                        $('#id_'+id).parent('tr').remove();
                        $('#code_message').html('Продукт удален!');
                        $('#dialog_message').dialog('open');
                        $('#dialog_change').dialog( "close" );
                     }else{
                         alert(result);
                            $('#code_message').html('Произошла ошибка!');
                            $('#dialog_message').dialog('open');
                     }
		}
       }); 
 }
 function changeCat(id){
    var category = $('#'+id).val();
    var id_product = id.substring(id.indexOf('_')+1, 10); 
       
     $.ajax({
		type: 'POST',
		url : '/back_end.php',
		data: 'cmd=changeCat&id='+id_product+'&category='+category,
		success: function (result_query) {
                    if(result_query){
                        $('#code_message').html('Данные успешно изменены!');
                        $('#dialog_message').dialog('open');
                    }else{
                         alert(result);
                            $('#code_message').html('Произошла ошибка!');
                            $('#dialog_message').dialog('open');
                     }
		}
       }); 
 }
 
        
        
    

