<?php
    interface IProductDB {
        function saveProduct ($title, $category,  $sum);
        function getProduct ();
        function deleteProduct ($id);
        function updateProduct ($title, $category, $sum, $id);
    }
?>